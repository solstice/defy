# README #

The Deniable Encrypted Filesystem for YAFFS (DEFY)

### What is DEFY? ###
* DEFY is a deniable encryption for flash devices.  It provides an arbitrary number of security levels, authenticated encryption, and secure deletion.  
* The code here is based on both YAFFS (http://www.yaffs.net/download-yaffs-using-git) and WhisperYAFFS (https://github.com/WhisperSystems/WhisperYAFFS).

### Setup (for Ubuntu Linux) ###
* Rebuild the Linux kernel to allow the loading of unsigned kernel modules: https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel

### Building and Using DEFY ###
* Build the mounter program in the mounter directory and copy "mounter" to the main directory
* Run the "loadDEFY.sh" script
* If reloads are desired, run the "reloadDEFY.sh" script -- this will reset the entire filesystem