/*
 * YAFFS: Yet Another Flash File System. A NAND-flash specific file system.
 *
 * Copyright (C) 2002-2011 Aleph One Ltd.
 *   for Toby Churchill Ltd and Brightstar Engineering
 *
 * Created by Charles Manning <charles@aleph1.co.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "linux/random.h"

#include "yaffs_guts.h"
#include "yaffs_trace.h"
#include "yaffs_packedtags2.h"

#include "yaffs_crypto.h"

// A simple function to generate a crc value based on the contents of tags.
unsigned yaffs_tags_marshall_compute_crc(const u8 *data, unsigned data_length) {
    unsigned computed_crc = 0;
    const u8 *ptr;
    
    for (ptr = data; ptr < data + data_length; ++ptr) {
        computed_crc = computed_crc ^ (*ptr);
    }

    return computed_crc;
}


static int yaffs_tags_marshall_dummy_write(struct yaffs_dev *dev,
                    int nand_chunk) 
{
    struct yaffs_packed_tags2 pt; 
	int packed_tags_size =
	    dev->param.no_tags_ecc ? sizeof(pt.t) : sizeof(pt);
	void *packed_tags_ptr =
	    dev->param.no_tags_ecc ? (void *)&pt.t : (void *)&pt;

    // a yaffs_temp_buffer is chunk sized buffer
    u8 *chunk_bytes = yaffs_get_temp_buffer(dev);
    int retval, i;

    if (!chunk_bytes) {
        yaffs_trace(YAFFS_TRACE_ALWAYS, "allocation error!");
        return -1;
    }

    // temporarily set one of the packed tags to something recognizable:
    get_random_bytes(chunk_bytes, dev->param.total_bytes_per_chunk);
    get_random_bytes(packed_tags_ptr, packed_tags_size);

    for (i = 0; i < dev->param.total_bytes_per_chunk; ++i) {
        chunk_bytes[i] = 0xAA;
    }
    for (i = 0; i < packed_tags_size; ++i) {
        ((char*) packed_tags_ptr)[i] = 0xDD;
    }

	yaffs_trace(YAFFS_TRACE_MTD,
		"yaffs_tags_marshall_dummy_write chunk %d, (%dx%d)",
		nand_chunk, dev->param.total_bytes_per_chunk, packed_tags_size);

	retval = dev->drv.drv_write_chunk_fn(dev, nand_chunk,
			chunk_bytes,
			dev->param.total_bytes_per_chunk,
			(dev->param.inband_tags) ? NULL : packed_tags_ptr,
			(dev->param.inband_tags) ? 0 : packed_tags_size);

	yaffs_release_temp_buffer(dev, chunk_bytes);
	return retval;
}

// Setting priv_level to a value > 0 tells this function to write 
//  the desired data at that privilege level.
static int yaffs_tags_marshall_write(struct yaffs_dev *dev,
				    int nand_chunk, const u8 *data,
				    const struct yaffs_ext_tags *tags, 
                    int priv_level, __u8 *page_stub)
{
    unsigned crc;
	struct yaffs_packed_tags2 pt;
	int retval;
	__u8 *encryptedData = NULL, *encryption_buffer = NULL;

	int packed_tags_size =
	    dev->param.no_tags_ecc ? sizeof(pt.t) : sizeof(pt);
	void *packed_tags_ptr =
	    dev->param.no_tags_ecc ? (void *)&pt.t : (void *)&pt;

	yaffs_trace(YAFFS_TRACE_MTD,
		"yaffs_tags_marshall_write chunk %d data %p tags %p",
		nand_chunk, data, tags);

    // compute and add a crc for the data to the header:
    crc = yaffs_tags_marshall_compute_crc(data, dev->param.total_bytes_per_chunk);

	/* For yaffs2 writing there must be both data and tags.
	 * If we're using inband tags, then the tags are stuffed into
	 * the end of the data buffer.
	 */
	if (!data || !tags)
		BUG();
	else if (dev->param.inband_tags) {
		struct yaffs_packed_tags2_tags_only *pt2tp;
		pt2tp =
		    (struct yaffs_packed_tags2_tags_only *)(data + dev-> data_bytes_per_chunk);
		yaffs_pack_tags2_tags_only(pt2tp, tags);

        // add the crc
        pt2tp->crc = crc;
	} else {
		yaffs_pack_tags2(&pt, tags, !dev->param.no_tags_ecc);
        // add the crc
        pt.t.crc = crc;
	}

	if (dev->isEncryptedFilesystem) {
		if (dev->param.inband_tags)
			BUG();

        if (priv_level < 0 || priv_level >= dev->num_privilege_levels) {
          yaffs_trace(YAFFS_TRACE_ALWAYS, "Invalid input level for encryption! (%d)", priv_level);
          BUG();
          return YAFFS_FAIL;
        }

		encryptedData = yaffs_get_temp_buffer(dev);
        encryption_buffer = yaffs_get_temp_buffer(dev);
		memcpy(encryptedData, data, dev->param.total_bytes_per_chunk);
        
        if (priv_level < 0 || priv_level == YAFFS_NO_PRIV_LEVEL || priv_level >= YAFFS_MAX_NUM_LEVELS) {
            yaffs_trace(YAFFS_TRACE_MTD, "bad priv_level: [%d]", priv_level);
            BUG();
        }
        else {
        }
        //yaffs_trace(YAFFS_TRACE_ALWAYS, "tagsMar Wrote: %d:%d [%d], crc: %x, seq %d, cId: %d", nand_chunk, page_stub[0], priv_level, crc, tags->seq_number, tags->chunk_id);

        // TODO: fix tags thing
        // yes, encrypted data is the input and output of this function...
        AON_encrypt(dev->ciphers[priv_level], dev->hashes[priv_level], encryptedData, encryptedData, encryption_buffer, 
                nand_chunk*2, dev->param.total_bytes_per_chunk, packed_tags_ptr+SEQUENCE_OFFSET, packed_tags_ptr+SEQUENCE_OFFSET,
                nand_chunk*2+1, packed_tags_size-SEQUENCE_OFFSET, page_stub);
	}
    else {
        yaffs_trace(YAFFS_TRACE_ALWAYS, "Wrote non-encrypted block!");
    }

	retval = dev->drv.drv_write_chunk_fn(dev, nand_chunk,
			(dev->isEncryptedFilesystem) ? encryptedData : data,
			dev->param.total_bytes_per_chunk,
			(dev->param.inband_tags) ? NULL : packed_tags_ptr,
			(dev->param.inband_tags) ? 0 : packed_tags_size);

    if (dev->isEncryptedFilesystem && encryptedData) {
        yaffs_release_temp_buffer(dev, encryptedData);
        yaffs_release_temp_buffer(dev, encryption_buffer);
    }
	// end added 

	return retval;
}

static int yaffs_tags_marshall_read(struct yaffs_dev *dev,
				   int nand_chunk, u8 *data,
				   struct yaffs_ext_tags *tags, int priv_level,
                   __u8 *page_stub)
{
    unsigned crc = 0;
	int retval = YAFFS_OK, decryptOk;
	int local_data = 0;
	u8 spare_buffer[100];
	enum yaffs_ecc_result ecc_result;

	struct yaffs_ext_tags placeholderTags;
	__u8 *encryptedData = NULL, *encryption_buffer = NULL;

	struct yaffs_packed_tags2 pt;

    // collect tag info (pt.t) or use tags and ecc information
    // For whisper yaffs we must manage the ecc information so we will use only (pt.t)
	int packed_tags_size =
	    dev->param.no_tags_ecc ? sizeof(pt.t) : sizeof(pt);
	void *packed_tags_ptr =
	    dev->param.no_tags_ecc ? (void *)&pt.t : (void *)&pt;

	yaffs_trace(YAFFS_TRACE_MTD,
		"yaffs_tags_marshall_read chunk %d:%d data %p tags %p, [%d]",
		nand_chunk, page_stub[0], data, tags, priv_level);
    if (dev->param.inband_tags) {
		if (!data) {
			local_data = 1;
			data = yaffs_get_temp_buffer(dev);
		}
	}

    // make space for encrypted fs
    if (dev->isEncryptedFilesystem) {
        encryptedData = yaffs_get_temp_buffer(dev);
        encryption_buffer = yaffs_get_temp_buffer(dev);
	  
	  if (!tags)
		tags = &placeholderTags;
	}

	if (dev->param.inband_tags || (data && !tags)) {
		if (dev->isEncryptedFilesystem) {
			BUG();
		}

		// perform as before or match whisper code of non-oob read?
		retval = dev->drv.drv_read_chunk_fn(dev, nand_chunk,
					data, dev->param.total_bytes_per_chunk,
					NULL, 0,
					&ecc_result);
	} else if (tags) {
        retval = dev->drv.drv_read_chunk_fn(dev, nand_chunk,
                (dev->isEncryptedFilesystem) ? encryptedData : data,
                (encryptedData || data) ? dev->param.total_bytes_per_chunk : packed_tags_size,
                spare_buffer, packed_tags_size,
                &ecc_result);

	} else
		BUG();

	if (dev->param.inband_tags) {
		if (dev->isEncryptedFilesystem)
			BUG();

		if (tags) {
			// packed_tags2_tags_only is to be used only with inband tags:
			struct yaffs_packed_tags2_tags_only *pt2tp;
			pt2tp =
				(struct yaffs_packed_tags2_tags_only *)
				&data[dev->data_bytes_per_chunk];

			yaffs_unpack_tags2_tags_only(tags, pt2tp);
		}
	} else if (tags) {
		memcpy(packed_tags_ptr, spare_buffer, packed_tags_size);
		
		if (dev->isEncryptedFilesystem) {			  

            if (pt.t.seq_number != 0xFFFFFFFF) {
                if (priv_level < 0 || priv_level >= dev->num_privilege_levels) {
                    yaffs_trace(YAFFS_TRACE_ALWAYS, "Invalid input level for decryption! (%d)", priv_level);
                    BUG();
                    return YAFFS_FAIL;
                }

                decryptOk = AON_decrypt(dev->ciphers[priv_level], dev->hashes[priv_level], encryptedData, encryptedData, encryption_buffer, 
                        nand_chunk*2, dev->param.total_bytes_per_chunk, packed_tags_ptr+SEQUENCE_OFFSET, packed_tags_ptr+SEQUENCE_OFFSET, 
                        nand_chunk*2+1, packed_tags_size-SEQUENCE_OFFSET, page_stub);

                if (decryptOk < 0) {
                    yaffs_trace(YAFFS_TRACE_ALWAYS, "read decrypt Fail %d:%d", nand_chunk, page_stub[0]);
                    BUG();
                }

                yaffs_unpack_tags2(tags, &pt, !dev->param.no_tags_ecc);
                crc = yaffs_tags_marshall_compute_crc(encryptedData, dev->param.total_bytes_per_chunk);
                if (tags->crc == crc) {
                    yaffs_trace(YAFFS_TRACE_ALWAYS, "read encrypted %d at %d:%d crc: %x", nand_chunk, priv_level, page_stub[0], crc);
                }
                else {
                    // this block has no decryption:
                    yaffs_trace(YAFFS_TRACE_ALWAYS, "read (fail) %d at %d:%d crc: %x vs %x", nand_chunk, priv_level, page_stub[0], crc, tags->crc);
                }
            }
            else {
                yaffs_trace(YAFFS_TRACE_MTD, "all 0xFF seq block %d at %d:%d", nand_chunk, priv_level, page_stub[0]);
            }

			if (data) {
				memcpy(data, encryptedData, dev->param.total_bytes_per_chunk);
			}
			yaffs_release_temp_buffer(dev, encryptedData);
            yaffs_release_temp_buffer(dev, encryption_buffer);
		}
        yaffs_unpack_tags2(tags, &pt, !dev->param.no_tags_ecc);
	}

	if (local_data)
		yaffs_release_temp_buffer(dev, data);

	if (tags && ecc_result == YAFFS_ECC_RESULT_UNFIXED) {
		tags->ecc_result = YAFFS_ECC_RESULT_UNFIXED;
		dev->n_ecc_unfixed++;
	}

	if (tags && ecc_result == -YAFFS_ECC_RESULT_FIXED) {
		if (tags->ecc_result <= YAFFS_ECC_RESULT_NO_ERROR)
			tags->ecc_result = YAFFS_ECC_RESULT_FIXED;
		dev->n_ecc_fixed++;
	}

	if (ecc_result < YAFFS_ECC_RESULT_UNFIXED) {
		return retval;
    }
	else {
        yaffs_trace(YAFFS_TRACE_MTD, "ecc fail!");
		return YAFFS_FAIL;
    }
}

static int yaffs_tags_marshall_query_block(struct yaffs_dev *dev, int block_no,
			       enum yaffs_block_state *state,
			       u32 *seq_number)
{
	int retval;

	yaffs_trace(YAFFS_TRACE_MTD, "yaffs_tags_marshall_query_block %d",
			block_no);

	retval = YAFFS_OK; //dev->drv.drv_check_bad_fn(dev, block_no);

	if (retval== YAFFS_FAIL) {
		yaffs_trace(YAFFS_TRACE_MTD, "block is bad");

		*state = YAFFS_BLOCK_STATE_DEAD;
		*seq_number = 0;
	} else {
		struct yaffs_ext_tags t;

        // (yaffs_dev *dev, int NAND-chunk, u8 *data, yaffs_ext_tags *tags)
		yaffs_tags_marshall_read(dev,
				    block_no * dev->param.chunks_per_block,
				    NULL, &t, YAFFS_NO_PRIV_LEVEL, 0);

		if (t.chunk_used) {
			*seq_number = t.seq_number;
			*state = YAFFS_BLOCK_STATE_NEEDS_SCAN;
		} else {
			*seq_number = 0;
			*state = YAFFS_BLOCK_STATE_EMPTY;
		}
	}

	yaffs_trace(YAFFS_TRACE_MTD,
		"block query returns  seq %d state %d",
		*seq_number, *state);

	if (retval == 0)
		return YAFFS_OK;
	else
		return YAFFS_FAIL;
}

static int yaffs_tags_marshall_mark_bad(struct yaffs_dev *dev, int block_no)
{
	return dev->drv.drv_mark_bad_fn(dev, block_no);

}


void yaffs_tags_marshall_install(struct yaffs_dev *dev)
{
	if (!dev->param.is_yaffs2)
		return;

    if (!dev->tagger.write_dummy_chunk_tags_fn)
		dev->tagger.write_dummy_chunk_tags_fn = yaffs_tags_marshall_dummy_write;

	if (!dev->tagger.write_chunk_tags_fn)
		dev->tagger.write_chunk_tags_fn = yaffs_tags_marshall_write;

	if (!dev->tagger.read_chunk_tags_fn)
		dev->tagger.read_chunk_tags_fn = yaffs_tags_marshall_read;

	if (!dev->tagger.query_block_fn)
		dev->tagger.query_block_fn = yaffs_tags_marshall_query_block;

	if (!dev->tagger.mark_bad_fn)
		dev->tagger.mark_bad_fn = yaffs_tags_marshall_mark_bad;
}
