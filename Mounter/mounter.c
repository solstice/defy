#include <sys/mount.h>
#include <stdio.h>
#include <string.h>

#define NON_PASSWORD_ARGS 3

int main(int argc, char **argv) {
    const char *deviceName = "/dev/mtdblock0";
    const char *mountTarget = "/mnt/yaffs/";
    const char *filesystemType = "yaffs2";
    char mountOptions[] = 
"tags-ecc-off,disable-summary,no-checkpoint,num_fs_levels=#,create_encrypted=";
    char reMountOptions[] = 
"tags-ecc-off,disable-summary,no-checkpoint,num_fs_levels=#,unlock_encrypted=";
    char *mountOptionsPtr;
    char fullOptions[256];
    int mountOptionsNoPassword_pos = 42;
    int mountOptionsNumPassword_pos = 57;

    if (argc < 4) {
        printf("Mounting yaffs in non-encrypted mode\n");
        mountOptions[mountOptionsNoPassword_pos] = 0;
        strcpy(fullOptions, mountOptions); 
    }
    else {
        int numLevels = atoi(argv[2]);
        int curLen, i;
        // open the file system:
        if (tolower(argv[1][0]) == 'o') {
            mountOptionsPtr = reMountOptions;
        }
        else if (tolower(argv[1][0]) == 'c') {
            mountOptionsPtr = mountOptions;
        }
        else {
            printf("usage: %s <[o]pen/[c]reate> <numLevels> <passwords ...>\n", argv[0]);
            printf("%s, %s =%c %c, %s\n", argv[0], argv[1], argv[1][0], argv[1][1], argv[2]);
            return 0;
        }

        if (numLevels <= 0) {
            printf("Bad num levels (%d) !\n", numLevels);
            return 0;
        }
        printf("Mounting yaffs in encrypted mode\n");

        mountOptionsPtr[mountOptionsNumPassword_pos] = numLevels + '0';
        strcpy(fullOptions, mountOptionsPtr); 
        curLen = strlen(fullOptions);

        if (numLevels != argc - NON_PASSWORD_ARGS) {
            printf("invalid num args!\n");
        }

        for (i = 0; i < numLevels; ++i) {
            strcpy(fullOptions+curLen, argv[NON_PASSWORD_ARGS+i]);
            curLen += strlen(argv[NON_PASSWORD_ARGS+i]);
            fullOptions[curLen] = '.';
            ++curLen;
        }
        --curLen;
        fullOptions[curLen] = 0;
    }

    printf("mounter: %s\n", fullOptions);
    if (mount(deviceName, mountTarget, filesystemType, 0, fullOptions) != 0) {
        perror("Mount Yaffs Fail");
        printf("May need super user privileges!\n");
        return -1; 
    }
    printf("Mount successful!\n");
    
    return 0;
}

